#include <vector>
#include "algobot.h"

using namespace std;

// Ejercicio 1
vector<int> quitar_repetidos(vector<int> s) {
    set<int> se(s.begin(), s.end());
    return vector<int>(se.begin(), se.end());
}

// Ejercicio 2
vector<int> quitar_repetidos_v2(vector<int> s) {

    set<int> se(s.begin(), s.end());
    return vector<int>(se.begin(), se.end());
}


// Ejercicio 3
bool mismos_elementos(vector<int> a, vector<int> b) {
    return set<int>(a.begin(), a.end()) == set<int>(b.begin(), b.end());
}

// Ejercicio 4
bool mismos_elementos_v2(vector<int> a, vector<int> b) {
    return set<int>(a.begin(), a.end()) == set<int>(b.begin(), b.end());
}

// Ejercicio 5
map<int, int> contar_apariciones(vector<int> s) {
    vector<int> contar;
    for (int n : s) {
        contar.push_back(count(s.begin(),s.end(),n));
    }
    map<int,int> dicc;

    for (int i = 0; i < s.size(); i++) {
     //   dicc[s[i]] = contar[i];
        dicc.insert({s[i], contar[i]});
    }
    return dicc;
}

// Ejercicio 6
vector<int> filtrar_repetidos(vector<int> s) {
    vector<int> contar;
    vector<int> filtrado;
    for (int n : s) {
        contar.push_back(count(s.begin(),s.end(),n));
    }
    for (int i = 0; i < s.size(); i++) {
        if(contar[i] == 1){
            filtrado.push_back(s[i]);
        }
    }
    return filtrado;
}

// Ejercicio 7
set<int> interseccion(set<int> a, set<int> b) {
    set<int> res;
    for (int n : a) {
        if (b.count(n) == 1) {
            res.insert(n);
        }
    }
    return res;
}

// Ejercicio 8
set<int> gen_sets(int n, vector<int> s) {
    set<int> res;
    for (int i = 0; i < s.size(); ++i) {
        if (s[i] % 10 == n) {
            res.insert(s[i]);
        }
    }
    return res;
}

map<int, set<int>> agrupar_por_unidades(vector<int> s) {
    map<int, set<int>> res;
    for (int i = 0; i < 10; ++i) {
        if (!(gen_sets(i,s).empty())) {
            //res[i] = gen_sets(i,s);
            res.insert({i,gen_sets(i,s)});
        }
    }
    return res;
}

// Ejercicio 9
vector<char> traducir(vector<pair<char, char>> tr, vector<char> str) {
    for (int i = 0; i < str.size() ; ++i) {
        for (int j = 0; j < tr.size(); ++j) {
            if (tr[j].first == str[i]) {
                str[i] = tr[j].second;
                break;
            }
        }
    }
    return str;
}
set<LU> interseccion_libretas(set<LU> a, set<LU> b) {
    set<LU> res;
    for (LU n : a) {
        if (b.count(n) == 1) {
            res.insert(n);
        }
    }
    return res;
}
// Ejercicio 10
bool integrantes_repetidos(vector<Mail> s) {
    bool se_repiten = false;
    for (int i = 0; i < s.size() -1 ; ++i) {
        for (int j = i + 1; j < s.size() ; ++j) {
             if( (s[i].libretas() != s[j].libretas()) && (interseccion_libretas(s[i].libretas(), s[j].libretas()).size() != 0)){
                 se_repiten = true;
             }
        }

    }

    return se_repiten;
}

// Ejercicio 11
map<set<LU>, Mail> entregas_finales(vector<Mail> s) {
    map<set<LU>, Mail> m;
    for (int i = 0; i < s.size(); ++i) {
        if(m.count(s[i].libretas()) == 0 && s[i].adjunto())
        {
            m[s[i].libretas()] = s[i];
        }
        else{
            if(s[i].fecha() > m[s[i].libretas()].fecha() && s[i].adjunto())
            {
                m[s[i].libretas()] = s[i];
            }
        }
    }
    return m;
}